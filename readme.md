I do a lot of Full Stack development, Front end, Backend, even Server setup and 
architecture/design.  When I do front-end development, I find myself using a lot 
of common tools such as jQuery and Bootstrap for UI, and Ajax results regardless 
of the environment.  What always ends up happening is that I'll want or need to 
do an Ajax call, retrieve some list of data, and then cycle through it to build 
the HTML view.

While tools like Angular, Ember, and React are nice, not everybody has the time or knowledge
to fully refactor and re-write the application to support those libraries.  And 
I absolutely hate manually injecting HTML inside my javascript.  (Yes, React, 
I'm looking at you and your JSX)

Its ugly and prone to issues to have something like an ajax success: 
function(result) { and then loop through the results while building a giant html 
string that will be appended or injected into the DOM.  htmlInjector is a simple 
js library built in ES6 standards that allows for creation of standards based 
html as an object you can then inject into your page.  

The resulting HTML object can be re-used, re-defined, or you can create a new 
html object for another element.  Due to the stateless nature of the web, these 
objects are generally used once and discarded.  But I didn't want to get into 
singletons and shit like that.

With this library, I can use jQuery to ajax request some data, and format that 
data into an HTML object and inject it into the DOM (on the page) directly without
a bunch of fuss or re-factoring my application to support yet another framework
or library.

I'm working on a composer installer, and I'd consider this whole project as Alpha level
for now.  It does work stand-alone, but I want to test it in a laravel project before calling it good.

## HTML Injector REQUIRES ES6 compliant browsers and will fail otherwise! ##
