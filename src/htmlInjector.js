
class htmlInjector {

    /**
     * constructor defines object attributes
     * 
     * @todo - create simple tags like button, i, ?
     * @todo - consider any events?  Maybe bind?
     */
    constructor(elementId, elementClass, elementData = '', elementAria = '') {
        this._elementId = elementId;
        this._elementClass = elementClass;
        this._elementData = elementData;
        this._elementAria = elementAria;
        this.html = '';
    }

    /**
     * Create an HTML List Element
     */
    listElement(type, listItems)
    {
        // validate type
        if (type.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 11: List Type not defined, use 'ol' or 'ul'.</p>";
            return;
        }
        
        // validate listItems
        if (listItems.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 12: List Items not set.</p>";
            return;
        }
        
        // validate listItems is an array
        if (typeof listItems !== 'object') {
            this.html = "<p class='alert alert-danger'>Error 13: Form field must be an array containing field information including label, field type, id, class, name, placeholder, value, and required.</p>";
            return;
        }
        
        switch (type) {
            case 'ul':
                var openTag = "<ul id='" + this._elementId + "' class='" + this._elementClass + "'>";
                var closeTag = "</ul>";
                break;

            case 'ol':
                var openTag = "<ol id='" + this._elementId + "' class='" + this._elementClass + "'>";
                var closeTag = "</ol>";
                break;

            default:
                this.html = "<p class='alert alert-danger'>Error 09: List Element Type not specified!  Specify ol or ul element type.</p>";
                return;
                break;
        }

        this.html += openTag;
        for (var i = 0; i < listItems.length; i++) {
            this.html += "<li class='list-group-item'>" + listItems[i] + "</li>";
        }
        
        this.html += closeTag;
    }

    /**
     * create a <p> element
     */
    pElement(text)
    {
        var html = "<p";
        if (this._elementId.length > 0) {
            html += " id='"+this._elementId+"'";
        }
        
        if (this._elementClass.length > 0) {
            html += " class='"+this._elementClass+"'"
        }
        this.html = html + ">" + text + "</p>";
    }

    /**
     * create an empty div element
     */
    divElement()
    {
        var html = "<div";
        if (this._elementId.length > 0) {
            html += " id='"+this._elementId+"'";
        }
        
        if (this._elementClass.length > 0) {
            html += " class='"+this._elementClass+"'"
        }
        this.html = html + "></div>";
    }

    /**
     * create a span element with text
     */
    spanElement(text)
    {
        var html = "<span";
        if (this._elementId.length > 0) {
            html += " id='"+this._elementId+"'";
        }
        
        if (this._elementClass.length > 0) {
            html += " class='"+this._elementClass+"'"
        }
        this.html = html + ">" + text + "</span>";
    }

    /**
     * create a heading element
     * @param integer size
     * @param string text
     */
    headingElement(size, text)
    {
        // validate size
        if (size.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 6: heading size not set.</p>";
            return;
        }
        
        // validate heading text
        if (text.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 1: Missing Heading Text.</p>";
            return;
        }
        
        var html = "<h"+size;
        if (this._elementId.length > 0) {
            html += " id='"+this._elementId+"'";
        }
        
        if (this._elementClass.length > 0) {
            html += " class='"+this._elementClass+"'"
        }
        this.html = html + ">" + text + "</h>";
    }

    /**
     * create an img tag
     * @param string source
     * @param string alt
     */
    imageElement(source, alt)
    {
        // validate source
        if (source.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 4: Image source not set.</p>";
            return;
        }
        
        var html = "<img";
        if (this._elementId.length > 0) {
            html += " id='"+this._elementId+"'";
        }
        
        if (this._elementClass.length > 0) {
            html += " class='"+this._elementClass+"'"
        }
        
        this.html = html + " src='"+source.toString()+"' alt='"+alt.toString()+"'/>";
    }

    /**
     * create a table by looping through arrays for headings and table rows
     * @param headings array
     * @param rows array
     */
    tableElement(headings, rows)
    {
        // validate headings
        if (headings.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 2: table headings are missing.</p>";
            return;
        }
        
        // validate form fields is an array
        if (typeof headings !== 'object') {
            this.html = "<p class='alert alert-danger'>Error 10: Table headings is not an array</p>";
            return;
        }
        
        var html = "<table";
        if (this._elementId.length > 0) {
            html += " id='"+this._elementId+"'";
        }
        
        if (this._elementClass.length > 0) {
            html += " class='"+this._elementClass+"'"
        }
        
        this.html = html + "><thead>";
        
        for (var i = 0; i < headings.length; i++) {
            this.html += "<th>" + headings[i] + "</th>";
        }
        this.html += "</thead><tbody>";
        
        if (rows.length > 0) {
            var rowHtml = '';
            for (var i = 0; i < rows.length; i++) {
                rowHtml += "<tr>";
                for (var n=0; n<rows[i].length; n++) {
                    rowHtml += "<td>"+rows[i][n]+"</td>";
                }
                rowHtml += "</tr>";
            }
            this.html += rowHtml;
        }
        this.html += "</tbody></table>";
    }
    
    /**
     * create a form element
     * @TODO
     * @param  string action 
     * @param string method
     * @param array fields
     */
    formElement(action, method, fields)
    {
        // validate action
        if (action.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 7: Form attribute requires an action.</p>";
            return;
        }
        
        // validate method is set
        if (method.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error: 8: method attribute requires a method.</p>";
            return;
        }
        
        /**
         * define available/supported methods methods
         * @type Array
         */
        var methods = ["POST", "GET", "PUT", "DELETE", "PATCH"];
        
        // validate method is supported
        if (methods.indexOf(method) == -1) {
            this.html = "<p class='alert alert-danger'>Error 6: Form method must be one of "+methods.toString()+".</p>";
            return;
        }
        
        // validate form fields defined
        if (fields.length <= 0) {
            this.html = "<p class='alert alert-danger'>Error 5: Form fields are required.</p>";
            return;
        }
        
        // validate form fields is an array
        if (typeof fields !== 'object') {
            this.html = "<p class='alert alert-danger'>Error 10: Form field must be an array containing field information including label, field type, id, class, name, placeholder, value, and required.</p>";
            return;
        }
    }
    
    /**
     * Create a button with a class, onclick event, and label
     * 
     * @param string label
     * @param string event
     * @returns {undefined}
     */
    buttonElement(label, id='', cssClass='', event='')
    {
        var html = "<button";
        
        if (id != '') {
            html += "id='"+id+"'";
        }
        
        if (cssClass != '') {
            html += " class='btn "+cssClass+" "+this._elementClass+"'";
        } else {
            html += " class='btn btn-default "+this._elementClass+"'";
        }
        
        if (event != '') {
            html += " onclick='"+event+"'";
        }
        
        html += ">"+label+"</button>";
        
        this.html = html;
    }
}
