$(document).ready(function() {
	// create a new div element
	var newDiv = new htmlInjector('myDiv', 'classy');
	newDiv.divElement();
	$("#newdiv").append(newDiv.html);

	// put a new heading element inside it
	var heading = new htmlInjector('myHeading', '');
	heading.headingElement(2, 'This is a Heading');
	$("#myDiv").append(heading.html);

	var span = new htmlInjector('mySpan', '');
	span.spanElement('A span followed by an unordered list');
	$("#span").append(span.html);
	
	// create a list item
	var listItems = ["apples", "oranges", "pears", "mangos", "pineapple"];
	var html = new htmlInjector('unordered_list', 'list-group col-md-3');
	html.listElement('ol', listItems);
	$("#list").append(html.html);

	//var html = new htmlInjector('', '');
	html._elementId = '';  // reset element attributes
	html._elementClass = '';
        
	html.pElement('This paragraph was added AFTER the list was added, using the same class object and will inherit the same id and class unless reset.');
	$("#list").after(html.html);
        
        html._elementId = 'myTable';  // reset element attributes
	html._elementClass = 'table table-striped';
        
        var headings = ["id", "animal", "likes"];
        var rows = [
            ["1", "Cat", "666"],
            ["2", "Bird", "118"],
            ["3", "Dog", "436"],
            ["4", "rodents", "56"],
            ["5", "Reptiles", "87"]
            
        ]
        html.tableElement(headings, rows);
	$("#list").after(html.html);
        
        // buttons
        var button = new htmlInjector('buttons', 'col-sm-2');
        button.buttonElement('Save', '', 'btn-info', '');
        $("#buttons").append(button.html);
        button.buttonElement('Delete', '', 'btn-danger', 'warnMe();');
        $("#buttons").append(button.html);
});


function warnMe()
{
    alert('a button onclick event');
}